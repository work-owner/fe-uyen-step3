
var xslide = 2;
function plusSlides(n) {
  xslide = n + xslide;
  showSlides(xslide);
}
function showSlides(n) {
  var slides = document.getElementsByClassName("list");
  if (xslide > slides.length - 1) {
    xslide = slides.length - 1;
  } else if (xslide < 2) {
    xslide = 2;
  } else {
    for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
    }
    slides[xslide - 2].style.display = "block";
    slides[xslide - 1].style.display = "block";
    slides[xslide].style.display = "block";
  }
}